import React, { Component } from 'react';
import './App.css';
import Nvebar from './Nvebar';

export default class  Counter extends Component{
    constructor(props){
        super()
        this.state={count:0}
        console.log('app-constructor')
    }
    handleReset(){
        this.setState({count:null})
    }
    componentDidMount(){
        console.log("app-mounted")
    }
    componentDidUpdate(preProps,preState){
        console.log("APP-UPDATED")
        console.log("App preProps",preProps)
        console.log("App-preState",preState)
    }
    increse(){
        let count=this.setState.count
        this.setState({count:count+=1})

    }
    decrese(){
        let count=this.setState.count
        this.setState({count:count-=1})

    }
    render(){
        console.log("APP-RENDERD")
        
        return(

            <div className="App">
                <Nvebar></Nvebar>
                <header className="App-header">
                    {
                        <main role="main" className="container">
                            <h1>Countter</h1>
                            <div>
                                {<h2>{this.state.count}</h2>}
                                <div>
                                    <button onClick={this.increse.bind(this)}className="btn btn-preProps"></button>
                                    <button onClick={this.decrese.bind(this)}className="btn btn-preState"></button>
                                    <button onClick={this.handleReset.bind(this)}className="btn btn-prehandleReset"> </button>
                                </div>
                            </div>
                        </main>
                    }
                </header>
            </div>
            
        )
        

    }
}
